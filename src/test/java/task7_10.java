import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class task7_10 {
    @Test
    void task10() throws InterruptedException {
        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().fullscreen();
        try {
            webDriver.get("https://demoqa.com/alerts");
            webDriver.findElement(By.xpath("//*[@id=\"alertButton\"]")).click();
            Thread.sleep(3000);
            webDriver.switchTo().alert().accept();//Task7
        } finally {
            webDriver.close();
        }
        Thread.sleep(4000);


        try {
            webDriver.get("https://demoqa.com/buttons");
            Actions actions = new Actions(webDriver);
            WebElement doubleClick = webDriver.findElement(By.xpath("//*[@id=\"doubleClickBtn\"]"));
            actions.doubleClick(doubleClick).perform();//Task8-9

        } finally {
            webDriver.close();

        }
        Thread.sleep(4000);


        try {
            webDriver.get("https://demoqa.com/upload-download");
            webDriver.findElement(By.xpath("//*[@id=\"downloadButton\"]")).click();
            WebElement upload = webDriver.findElement(By.xpath("//*[@id=\"uploadFile\"]"));
            upload.sendKeys("C:\\Users\\hp\\Desktop\\IBA_LESSONS\\IBA 1_3 .docx");
        } finally {
            webDriver.close();
        }


    }
}
