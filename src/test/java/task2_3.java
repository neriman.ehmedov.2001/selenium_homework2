import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class task2_3 {
    @Test
    void task3() throws InterruptedException {
        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().fullscreen();
        try {
            webDriver.get("http://demo.guru99.com/test/guru99home/");
            webDriver.switchTo().frame("a077aa5e");
            Thread.sleep(2000);
            System.out.println("********We are switch to the iframe*******");
            Thread.sleep(2000);
            webDriver.findElement(By.xpath("html/body/a/img")).click();
            Thread.sleep(2000);
            System.out.println("*********We are done***************");

        } finally {

        }

    }
}
