import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class task5_6 {
    @Test
    void task6() {
        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();
        try {
            webDriver.get("https://www.sugarcrm.com/uk/request-demo/");
            WebElement month_drop = webDriver.findElement(By.name("employees_c"));
            Select dropdown = new Select(month_drop);
            dropdown.selectByValue("level4");//task 5-6
        } finally {
            webDriver.close();
        }
    }
}
