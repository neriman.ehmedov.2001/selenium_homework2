import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Task1 {
    @Test
    void task() throws InterruptedException {
        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();
        webDriver.get("https://demoqa.com/browser-windows");
        webDriver.findElement(By.xpath("//*[@id=\"tabButton\"]")).click();// task 1-2
        String expectedResult = "This is a sample page";
        assertEquals(expectedResult, webDriver.findElement(By.xpath("//*[@id=\"sampleHeading\"]")).getText());
    }
}
